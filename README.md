# Invitation letter

Dear RoboCon attendee,

my name is Markus Stahl and I welcome you to this workshop about *[Integrating Robot Framework to devops infrastructure](https://robocon.io/#integrating-robot-framework-to-devops-infrastructure)*.

During this course, we will set up a software development infrastructure used in every software developing business. Since the choice of tools is wide, but similar, we are going to focus on some most common open source tools. However, we assume any competitive tool you use to be similar. Differences between tools may be discussed during the course.

Our set up will be on the following tools:
1. Git
1. Gitlab
1. Gitlab CI
2. Robotframework Lint
1. (Jenkins)
1. PostgreSql
2. DbBot

At first we ask you to create an account at gitlab and make bookmark on the gitlab group created for this course.
1. [Link to description how to create an account at Gitlab](https://docs.gitlab.com/ee/user/profile/)
1. Bookmark course material in your browser: https://gitlab.com/robocon2020-rf-in-infrastructure

During the course, we will use slack for sharing code snippets. If you did not join robotframework on slack, yet, please consider doing at: https://robotframework-slack-invite.herokuapp.com/
There we will have a channel for this workshop at: https://app.slack.com/client/T07PJQ9S7/CRVHXP70S/details/info

If you have any question, do not hesitate to contact me:
Email: markus.stahl@postadress.de
LinkedIn: https://www.linkedin.com/in/markus-stahl-47385134
Xing: https://www.xing.com/profile/Markus_Stahl9

**The workshop will take place at [Technopolis Ruoholahti 2 , Energiakuta 3, Helsinki](https://goo.gl/maps/3Lq879tfC86BLnDPA) in room *Video Helsinki* starting at 9 am.**

I wish you all a pleasant journey and hope to see you all happy and well on Tuesday. 

Best regards,
Markus